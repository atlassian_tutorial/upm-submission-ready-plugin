package ut.com.example.plugin.tutorial;

import org.junit.Test;
import com.example.plugin.tutorial.MyPluginComponent;
import com.example.plugin.tutorial.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}